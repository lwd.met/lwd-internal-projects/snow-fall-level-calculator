# Dry snowfall level

This project intends to calculate and plot the dry snowfall level (DSL) from temperature and relative humidity automated-weather-station (AWS) data in order to indicate the elevation, below which melt-freeze-crusts are prone to form from rain-on-snow events. For a more in depth-description of the project, read our paper or have a look at our poster presented at the ISSW 2024. Both are stored in _/ISSW2024_.

**Definition dry snowfall level (DSL)**:
The elevation, where 100% of the precipitation falls as snow and is dry (= the elevation, above which there is no relevant moisture entry into the snowpack and hence no melt-freeze-crusts will develope from this specific precipiation event.)

## Calculation Approach

The approach uses 10min air temperature (T) and relative humidity (RH) from AWSs as input and calculates the maximal hourly dry snowfall level in two steps:

- **Step 1**: Calculate hourly max wet-bulb temperature (tpsy) from air temperature (T) and relative humidity (RH).

$$tpsy = -5,806 + 0,672 * T - 0,006 * T² + (0,061 + 0,004 * T + 0,000099 * T²) * RH + (-0,000033 - 0,000005 * T - 0,0000001 * T²) * RH²$$

- **Step 2**: Calculate dry snowfall level from the stations' elevation (h) and wet-bulb temperature (tpsy) assuming a tpsy of 0.3°C for the elevation of the DSL.

$$DSL = h + (tpsy - 0.3) / 0.006$$  _Steinacker (1983)_

## Package description

The python version used in this script is `Python 3.13.1`.  
For additional package requirements see [pyproject.toml](pyproject.toml).
We use [uv](https://github.com/astral-sh/uv) as a package manager to simplify collaboration using virtual environments.

**This package:**

* calculates the hourly maximal DSL for the last 7 days (by default. Other option available) and stores is to `./dsl_calculated.csv`
* loads the observed 'SnowLine' from admin.avalanche.report for the last 7 days (by default. Other option available).
* creates a timeseries plot for each microregion containing the DSL for all AWSs in that microregion as a lineplot as well as the observed 'SnowLine's as grey dots. Furthermore, the respective precipitation data for that microregion is plotted as a black dashed line. Hours with precipitation intensities >=0.1mm are shown with a background shading that (by default) varies with different precip intensities. 
* stores (at most) one geosjon-file per day. The name of the geojson that is saved, corresponds to the last day of the given timeperiod. The geojson-file includes one value per AWS showing the maximal DSL that occured during the last day of the plotted timeseries. Only DSL values with precipitation inensities >= 0.1mm at the timestep are taken into account for calculating the maximal DSL values. This implies, that on a day without precipitation, no geojson-file will be stored. On a day with precip only at some of the AWSs, the geojson-file will only include the AWSs with precipiation > 0mm.
* (optionally) validates the calculated DSLs with observed snowfall limites (SFL) from the official observers of LWD Tirol. Find more on this in the section [validation](#validation).

### Operational use

The easiest way is to install **uv**.

To run the package, use:

`uv run main.py --input_path [path_to_inputfile] --output_path [path_to_desired_output] --username_WISKI [API username] --password_WISKI [API password] --username_GUI [admin.gui username] --password_GUI [admin.gui password]`

Currently, we run the package daily within this git-repository with the following command:

`uv run main.py --input_path ./input/metadata_regen_einheitlich_proregion.xlsx --output_path ./output --username_WISKI [API username] --password_WISKI [API password] --username_GUI [admin.gui username] --password_GUI [admin.gui password]`

Command line required variables:

- `[path_to_inputfile]`: Path and name of the input file, which needs to be an excel sheet containing metadata information on the AWSs that should be used. Use `'./input/metadata_regen_einheitlich_proregion.xlsx'` for currently used input-data containing the LWD-Tirol AWSs. Since LWD AWSs usually do not measure liquid precipitation, we assigned one precipitation AWS for each microregion. The precipitation AWS metadata information is also stored in the input file. 
- `[path_to_desired_outputfolder]`: Path where the output should be stored to. Plots will be stored in `[path_to_desired_output]/Plots/[timeperiod]/[date]`, geojson-files will be stored in `[path_to_desired_output]/geojson/[date]`
- `[WISKI username]`: AWS data is loaded via an API request of the WISKI-database.The username used for the API data request. 
- `[WISKI password]`: AWS data is loaded via an API request of the WISKI-database.The password used for the API data request.
- `[admin.gui username]`: The observed snowlinesare loaded via an API request for https://admin.avalanche.report/. The username used for the API data request. 
- `[admin.gui password]`: The observed snowlines are loaded via an API request for https://admin.avalanche.report/. The username used for the API data request. 

You can specify further optional variables:

`python main.py --input_path [path_to_inputfile] --output_path [path_to_desired_output] --username [API username] --password [API password] --timeperiod [chosen_period] --p_intensity [p_int yes/no] --tw_threshold' [tw_thresh] --username_GUI [admin.gui username] --password_GUI [admin.gui password]`

- `[chosen_period]`: The period for which you want to plot the data. Default is `w` (last 7 days, 00:00 - 23:00). Other options: `t` (today), `m` (last month / 31 days), `date1,date2`: a manually given period date1/2 given in the form yyyy-mm-dd, e.g. `2024-01-01,2024-01-05`.

- `[p_int yes/no]`: Whether you want the colors of precip background-shading to vary with intensity (`yes`) or not (`no`). Default is `yes`. If `no`, precip shading will turn on for precip >= 0.1mm/h, but will be same color for all intensities.
If `yes`, the shading will vary as follows:
        lightest_blue >= 0.1mm/h
        light_blue >= 0.5mm/h
        blue >= 4mm/h
        dark_blue >= 10mm/h

- `[tw_thresh]`: The wet-bulb temperature threshold used to seperate rain from snow (i.e. the wet-bulb temperature below which we assume all precip to be snow). Default is 0.3°C (Fehlmann et al. 2018).

### Validation
A seperate script `evaluation.py` handles the task of validating the calculated DSLs with observer data.

The script can be used as follows:
`python evaluation.py --input_path [path_to_inputfile] --output_path [path_to_desired_output] --username [API username] --password [API password] --timeperiod [chosen_period] --tw_threshold' [tw_thresh] --inputpath_obs [path_to_obs_inputfile] --eval_plottype [lin_reg/progression] --microregion1 [mr1] --microregion2 [mr2]` 

- all input variables that match the ones in the operational usecase can be used in the same manner for the evaluation
- `[path_to_obs_inputfile]`: The path to the file, where the seasons observed SFLs are stored (get them next season from Marco Knoflach/LO.LA). Default is: `'./input/Beobachterdaten_Regengrenzen_Tirol_23-24.xlsx'`. The file needs to be an excel-sheet, containing the regionnumber within the column `Region`, the date and time as a string within the column `TIMESTAMP` and the observed SFLs within a column named `DSL`.
- `[lin_reg/progression]`: The type of validation-plot that will be produced from your input variables. Default is `lin-reg`. See the sectio on [linear regression](#linear-regression) for more info. Other options: `progression`. See the section on [progression](#progression) for more info.
- `[mr1]`: Used only for the progression plottype. Specifies the first microregion, you want to plot the progression for. Default: `'AT-07-15 Westliche Tuxer Alpen'`. 
- `[mr2]`: Used only for the progression plottype. Specifies second microregion, you want to plot the progression for. Default: `'AT-07-22 Zentrale Stubaier Alpen'`. 


**Linear regression**

To run the existing validation for the season 2023-2024 in the form of a linear regression, use:
`python evaluation.py --input_path ./input/metadata_regen_einheitlich_proregion.xlsx --output_path ./output --username lwdtirol_snow_fall_level --password [password] --timeperiod '2023-11-01,2024-05-01'`

This will, for the whole season of 2023-11-01 - 2024-05-01, 
* plot a scatterplot of calculated DSLs and observed SFLs. 
* additionally in the same plot, show a linear regression between the two variables and 
* calculate an R²-value for the dataset and show it within the plot. 
* show a perfect linear regression as a grey dashed line.

To obtain this plot, observed SFLs are only used, if there was precipitation >= 0mm/h at the time of the observation. For the comparison to a specific observation, we use the median DSL of all AWSs within the region of the respective observation at the respective timestep.
AWS data is provided in wintertime for the entire year, whereas observed SFLs are reported in summertime in between the last sunday of march and the last sunday of october (conventions for switch between summer and wintertime). We correct for this by subtracting one hour from the observed SFL' timestamp if the observation took place in between these two dates.

Makes sense to specify a long timeperiod or a whole winterseason, in order to have enough data for a reasonable comparison and regression.

**Progression**

A second approach to validate the DSLs with observed SFLs is to run:
`python evaluation.py --input_path ./input/metadata_regen_einheitlich_proregion.xlsx --output_path ./output --username lwdtirol_snow_fall_level --password [password] --timeperiod [timeperiod] --eval_plottype progression --microregion1 [mr1] --microregion2 [mr2]`

This will 
* plot the same type of plot as in the operational usecase
* in the form of a 2x1 subplot, with DSLs shown for the microregion `[mr1]`in the upper subplot and `[mr2]` in the lower subplot
* additionally to the operational usecase, observed SFLs within the shown timeperiod will be plotted as grey dots

Do not select a timeperiod any longer than 14 days for the plot to still look reasonable.


**Existing ideas for further validation**

- Observe specific events: On different snowfall-events during the season: have multiple observers observe the DSL/melt-freeze-crust formation as precisely as possible. Find a draft on detecting melt-freeze-crusts via field-work after specific, appropriate rain-on-snow events here: 
*/X:/Daten/Projekte/DrySnowfallLevel/validation/DSL_validation_fieldwork*.

- Disdrometer data: There is a laser-sensor at Weissfluhjoch, Davos which can precisely measure whether precipitation is falling as snow, mixed or rain.
Idea: find those times, where the sensor detects the very start of the "real" snowfall. According to our approach, at that specific timestamp, tpsy should be 0.3°C. Validate this threshhold!
The swiss team provided us the data for this idea, which is stored in _X:/Daten/Projekte/DrySnowfallLevel/validation/disdrometer_.
So far, we did not work with the data.

### Python Development Setup
We use uv as package manager and to handle virtual environments allowing all collaborators to use identical
setups in terms of which versions of Python packages are being used. This ensures that the program always behaves the same when run on different machines. 
Moreover, IDEs like VS Code are able to use the virtual environment to provide more intelligent hints (syntax highlighting, type annotations, warnings, ...). And (if parameters are set correctly) one can use the run and debug functionlity of the IDE.

Project requirements are stated in the file `pyproject.toml`. The file `uv.lock`, is used to synchronize environments via locked dependencies. This ensures that all collaborators or deployment systems can work with identical setups.

#### Important commands
- run code: `uv run ...` (also creates/updates the virtual environment)
- just create/update virtual environment: `uv sync` 
- use the virtual environment in VS Code: `ctrl+shift+p` -> type in `python interpreter` -> select `Python: Select Interpreter` -> select the `.venv` inside the project 
- upgrade all dependencies to their latest compatible versions: `uv lock --upgrade`

## Extra material
All additional material that is not found on this gitlab-repository can be found in locally at LWD-Tirol via _X:/Daten/Projekte/DrySnowfallLevel/_.
