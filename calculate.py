# import libarieshtt
import sys  # for reading the command inputs
from datetime import date, datetime, timedelta

import numpy as np
import pandas as pd

import read_data


def calculate_wetbulb(t_air, rh):
    """
    Calculates the wetbulb temperature
    from air-temperature and relative humidity.

    Parameters
    ----------
    dt_air : float
        Air-temperature in °C

    rh : float
        relative humidity in %

    Returns
    -------
    tw : float
        wet-bulb temperature in °C
    """
    tw = (
        -5.806
        + 0.672 * t_air
        - 0.006 * t_air**2
        + (0.061 + 0.004 * t_air + 0.000099 * t_air**2) * rh
        + (-0.000033 - 0.000005 * t_air - 0.0000001 * t_air**2) * rh**2
    )
    return tw


def calculate_DSL(data, stationheight, start, end, tpsy_threshold=0.3):
    """
    Takes a merged LT and LF panda dataframe (10-min data)
    and the stationheight (in m) as input and calculates the
    highest dry snowfall level for each hour from hourly
    Tmax values and the corresponding LF.
    Returns a pandas dataframe with hourly maximal dry snowfall levels.


    Parameters
    ----------
    data : pandas dataframe
        needs to consist of a datetime as index called 'TIMESTAMP' and the two
        variables LT and LF as columns
    stationheight : float
        Stationheight of the Station that snowfall limit is calculated for
    start : datetime
        Starting date as datetime object
    ende : datetime
        End date as datetime object
    tpsy_threshold : float
        The wet-bulb temperature theshold used to seperate rain from snow
        (i.e. the wet-bulb temperature below which we assume all precip to be snow).
        Default: 0.3

    Returns
    -------
    df_hourlymax : pandas dataframe
        Hourly dataframe, similar to data, now also including the new variable
        'DSL' that shows the calculated snowfall limit
    """
    # calculate wetbulb-temperature from air temp and RH
    data["tpsy"] = calculate_wetbulb(data["LT"], data["LF"])
    # resample to hourly time resolution using the hourly maximal tpsy value
    df_hourlymax = data.drop(columns=["LT", "LF"]).resample("h").max()
    # calculate DSL
    df_hourlymax["DSL"] = (
        (df_hourlymax["tpsy"] - tpsy_threshold) / 0.006
    ) + stationheight

    # make sure there is data for each hour of the day
    di = pd.date_range(
        start=start, end=end + timedelta(days=1), freq="h", inclusive="left"
    )
    df_hourlymax = df_hourlymax.reindex(di, fill_value=np.nan)
    df_hourlymax = df_hourlymax.rename_axis(index="TIMESTAMP")

    # add full hour precip data
    df_hourlymax = df_hourlymax.assign(
        N=data["N"][data.index.strftime("%M:%S") == ("00:00")]
    )
    # delete all other variables
    df_hourlymax = df_hourlymax[["DSL", "N"]]
    df_hourlymax = df_hourlymax.reset_index()

    return df_hourlymax


def period_to_startend(period):
    """
    Takes a period in string format as input and returns the
    start- and end-dates as datetime objects.

    Parameters
    ----------
    period : str
        must be either of the following options:
        't' (today), 'w' (last week / 7 days), 'm' (last month / 31 days)
        'date1,date2': a manually given period date1/2
        given in the form yyyy-mm-dd

    Returns
    -------
    start, end : tuple of datetime object
        start: the start-time of the period in datetime format.
        end: the end-time of the period in datetime format.
    """
    # yesterday
    if period == "y":
        start = end = date.today() - timedelta(days=1)
    # today
    elif period == "t":
        start = end = date.today()
    # last week
    elif period == "w":
        start = date.today() - timedelta(days=7)
        end = date.today() - timedelta(days=1)
    # last month
    elif period == "m":
        start = date.today() - timedelta(days=30)
        end = date.today() - timedelta(days=1)
    # for a manually entered date in string format
    elif len(period) == 21:
        date_format = "%Y-%m-%d"
        start = datetime.strptime(period.split(",")[0], date_format).date()
        end = datetime.strptime(period.split(",")[1], date_format).date()
    else:
        print(
            "Entered a non-existing period. Periods can be: [y], [t], [w], [m] or manual periods. Manual periods must be provided in the form [yyyy-mm-dd,yyyy-mm-dd]!"
        )
        sys.exit()
    return start, end


def allstations_DSL(user, password, metadata, period, tpsy_threshold=0.3):
    """
    Takes a pandas-dataframe metadata, calculates the hourly DSL
    for the last 7 days (by default) for all stationnumbers
    included in metadata and saves it to a pandas dataframe
    containing hourly DSL and N values.


    Parameters
    ----------
    user : str
        username for url-request of WISKI-API interface
    password : str
        password for url-request of WISKI-API interface
    metadata : pandas dataframe
          consisting of the 'Messstellennummer' as an index,
          a column named 'Höhe' showing the stations
          elevation above sealevel, a column named 'Laenge'
          and a column named 'Breite';
          a column named Messstellennummer_N (number of precip station)


    period : str
        Default: 'w'. DSL is calculated and stored for last 7 days.
        must be either of the following options:
        't' (today), 'w' (last week / 7 days), 'm' (last month / 31 days)
        'date1,date2': a manually given period
        date1/2 given in the form yyyy-mm-dd

     tpsy_threshold : float
        The wet-bulb temperature theshold used to seperate rain from snow
        (i.e. the wet-bulb temperature below which we assume all precip to be snow).
        Default: 0.3

    Returns
    -------
    station_df : pandas dataframe
        dataframe containing hourly DSL and N values
        ('Größtregion', 'Überregion', 'Region', 'Messtellenname',
        'TIMESTAMP' as indices;
        DSL, N, Messstellennummer,Laenge,Breite,Höhe,Messtellenname_N,
        Höhe_N, Messstellennummer_N as columns)
    """
    station_df = pd.DataFrame()
    start, end = period_to_startend(period)

    # for each station listed in meta
    for stationnr in metadata.index:
        # get LT & LF data
        df = read_data.get_data(user, password, stationnr, start, end)
        # get corresponding precip data
        stationnr_N = int(metadata.loc[[str(stationnr)]].Messstellennummer_N.iloc[0])
        df_N = read_data.get_data(user, password, stationnr_N, start, end, precip=True)
        # merge LT,LF and precip data into df
        df = pd.merge(df, df_N, left_index=True, right_index=True)

        # if loaded data is not empty and not all NaN
        if df.LT.isnull().values.all() == False & df.LF.isnull().values.all() == False:  # noqa: E712
            # calculate DSL
            df_DSL = calculate_DSL(
                df, metadata.Höhe[str(stationnr)], start, end, tpsy_threshold
            )
            df_DSL["Messstellennummer"] = stationnr
            df_DSL = pd.merge(
                df_DSL, metadata.loc[[str(stationnr)]], on="Messstellennummer"
            )
            # append data to dataframe
            station_df = pd.concat([station_df, df_DSL], ignore_index=True)
        # if loaded data is empty or all NaN create an all NaN DSL pandas dataframe
        else:
            continue

        station_df["N_sum_1hr"] = station_df["N"].diff()
        station_df["N_sum_1hr"] = station_df["N_sum_1hr"].mask(
            station_df["N_sum_1hr"] < 0, station_df["N"]
        )

    if station_df.empty:
        print(
            "All data is missing! Probably data query did not work. Cannot calculate DSLs."
        )
        sys.exit()
    else:
        station_df = station_df.set_index(["Region", "Messtellenname", "TIMESTAMP"])
    return station_df
