# import libaries
import os

import geopandas
import numpy as np
import pandas as pd


def calculate_daily_max(station_df, metadata):
    """
    Calculates the maximum DSL that occurs during precipitation >= 0.1mm/h
    of the day before the calculation and for each station.

    Parameters
    ----------
    station_df : pandas dataframe
        created by function 'allstations_DSL';
        'Region', 'Messtellenname', 'TIMESTAMP' as indices;
        DSL, N, Messstellennummer,Laenge,Breite,Höhe,Messtellenname_N,
        Höhe_N, Messstellennummer_N as columns
    metadata : pandas dataframe
          consisting of 'Messstellennummer' as an index,
          columns named 'Höhe','Laenge','Breite', 'Region',
          'Messtellenname_N','Höhe_N','Messstellennummer_N'

    Returns
    -------
    daily_max_meta : pandas dataframe
        'Messstellennummer' as index; 'Messtellenname',
        'Laenge', 'Breite', 'Höhe','Region'
        and daily maximal DSL as column ('DSL') as columns

    """
    # select only last day of provided timeseries data
    yesterday = pd.date_range(
        station_df.index.unique("TIMESTAMP")[-24],
        station_df.index.unique("TIMESTAMP")[-1],
        freq="1h",
    )
    df_y = station_df[station_df.index.isin(yesterday.values, level="TIMESTAMP")].copy()

    # add column with hourly precip differences
    df_y["N_sum_1hr"] = df_y["N"].diff()
    df_y["N_sum_1hr"] = df_y["N_sum_1hr"].mask(df_y["N_sum_1hr"] < 0, df_y["N"])

    # Setze die Differenz auf NaN, wenn sich der Messstellenname ändert
    index_changes = pd.Series(
        df_y.index.get_level_values("Messtellenname")
    ) != pd.Series(df_y.index.get_level_values("Messtellenname")).shift(1)
    df_y.loc[index_changes.values, "N_sum_1hr"] = np.nan

    # filter for daily max DSL during precip where hourly sum is >= 0.1mm/h
    daily_max = (
        df_y.where(df_y.N_sum_1hr >= 0.1)
        .groupby(["Messstellennummer"])["DSL"]
        .max()
        .round(0)
    )
    daily_max_meta = pd.merge(metadata, daily_max, on="Messstellennummer")
    daily_max_meta = daily_max_meta.drop(
        columns=["Höhe", "Messtellenname_N", "Höhe_N", "Messstellennummer_N"]
    )

    return daily_max_meta


def save_geojson(station_df, metadata, outputpath):
    """
    Saves the maximum daily DSL for yesterday to a geojson
    with the filename 'yyyy-mm-dd.geojson'.
    Does not work, if the timeperiod of station_df ends during the day.

    Parameters
    ----------
    station_df : pandas dataframe
        created by function 'allstations_DSL';
        'Region', 'Messtellenname', 'TIMESTAMP' as indices;
        DSL, N, Messstellennummer,Laenge,Breite,Höhe,
        Messtellenname_N,Höhe_N, Messstellennummer_N as columns
    metadata : pandas dataframe
          consisting of 'Messstellennummer' as an index,
          columns named 'Höhe','Laenge','Breite','Region',
          'Messtellenname_N','Höhe_N','Messstellennummer_N'
    outputpath : str
        Path where to store the output.

    Saves
    -------
    gdf_dailymax : geopandas dataframe
        Each feature is a station and includes the respective maximum daily DSL of the station as a property.

    """
    dailymax_df = calculate_daily_max(station_df, metadata)
    points = geopandas.points_from_xy(x=dailymax_df.Laenge, y=dailymax_df.Breite)
    # create geopandas dataframe from new DSL data
    gdf_dailymax = geopandas.GeoDataFrame(dailymax_df, geometry=points)
    gdf_dailymax = gdf_dailymax.drop(columns=["Laenge", "Breite"])
    gdf_dailymax = gdf_dailymax.rename(
        columns={
            "Messtellenname": "station_name",
            "Region": "subregion",
            "DSL": "snowfall_limit",
        }
    )
    gdf_dailymax.index = gdf_dailymax.index.rename("station_number")

    # add associated plot name to geojson file as an additional property
    gdf_dailymax["plot_name"] = gdf_dailymax.subregion.str.split(" ").str[0] + ".png"

    time = station_df.index.unique("TIMESTAMP")[-1]
    # yesterday = date.today() - timedelta(1)
    filename = time.strftime("%Y-%m-%d") + ".geojson"
    results_dir = outputpath + "/geojson/"
    if not os.path.isdir(results_dir):
        os.makedirs(results_dir)

    # schema = gpd.io.file.infer_schema(gdf_dailymax)
    if not gdf_dailymax.empty:
        gdf_dailymax.to_file(results_dir + filename, driver="GeoJSON")
