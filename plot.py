# import libaries
import os

import matplotlib as mpl
import matplotlib.dates as mdates
import matplotlib.patches as mpatches
import matplotlib.ticker as tck
import numpy as np
import pandas as pd
import scipy as sp
import seaborn as sns
from matplotlib import pyplot as plt
from matplotlib.ticker import MultipleLocator

# %matplotlib inline
mpl.rcParams.update({"axes.grid": True})
mpl.use("agg")


pd.options.mode.chained_assignment = None


def plot_settings(axes, regions, zeitspanne=None, station_df=None, ax_type="DSL"):
    """
    Does all the plot settings used in plot.

    Args:
        axes (matplotlib.axes.Axes):
            The axes that the settings will be used on.
        regions (string):
            The respective region precip should be plotted for.
        zeitspanne (string, optional):
            The timeperiod, for which the data is plotted.
            Can be one of the following:
            'daily', 'weekly', 'monthly', 'manual'. Defaults to None.
        station_df (pd.DataFrame, optional):
            If specified otherwise, it needs to be a pandas dataframe
            created by function 'allstations_DSL';
            'Region', 'Messtellenname', 'TIMESTAMP' as indices;
            DSL, N, Messstellennummer,Laenge,Breite,Höhe,
            Messtellenname_N,Höhe_N, Messstellennummer_N as columns.
            Defaults to None.
        ax_type (str, optional):
            'DSL': (applies settings for DSL data).
            'precip' (applies settings for precip data).
            Defaults to "DSL".
    """
    if ax_type == "DSL":
        axes.set_title(str(regions), fontdict={"size": 10})
        axes.set_xlabel("")
        axes.set_ylabel("Höhe [m]", fontdict={"size": 10})
        if zeitspanne == "weekly":
            # Labels of x-axis will be displayed in 'dd.m H:M' format at 00:00 and 12:00 timestamps.
            axes.xaxis.set_major_locator(mdates.HourLocator(np.arange(0, 24, 12)))
            axes.xaxis.set_major_formatter(mpl.dates.DateFormatter("%d.%b %H:%M"))
            # Minorticks at 06:00 and 18:00 timestamps
            axes.minorticks_on()
            axes.xaxis.set_minor_locator(mdates.HourLocator(np.array([6, 18])))
        else:
            # Text in the x-axis will be displayed in 'dd.m H:M' format.
            axes.xaxis.set_major_formatter(mpl.dates.DateFormatter("%d.%b %H:%M"))
            axes.minorticks_on()
            axes.xaxis.set_minor_locator(mdates.HourLocator())

        # Rotates and right-aligns the x labels so they don't crowd each other.
        for label in axes.get_xticklabels(which="major"):
            label.set(rotation=30, horizontalalignment="right")

        # Set the range of y-axis
        axes.set_ylim(0, 3500)
        # Set the range of x-axis
        axes.set_xlim(
            [
                station_df.index.get_level_values("TIMESTAMP")[0],
                station_df.index.get_level_values("TIMESTAMP")[-1],
            ]
        )
    elif ax_type == "precip":
        axes.set_ylabel("P [mm]")
        axes.set_ylim(0, 70)
        axes.yaxis.set_major_locator(MultipleLocator(10))
        axes.yaxis.set_minor_locator(tck.AutoMinorLocator())
        axes.tick_params(direction="in", which="both")
        axes.grid(False)
        axes.get_legend().remove()
        axes.set_zorder(1)
        axes.patch.set_visible(False)


def plot_precip(station_df, region, ax, p_intensity=True, p_shading=True):
    """
    Plots precip as a sns.lineplot on a twinned x-axis of the current axis.

    Parameters
    ----------
    station_df : pandas dataframe
        created by function 'allstations_DSL';
        'Region', 'Messtellenname', 'TIMESTAMP' as indices;
        DSL, N, Messstellennummer,Laenge,Breite,Höhe,Messtellenname_N,
        Höhe_N, Messstellennummer_N as columns
    region : string
        The respective region precip should be plotted for.
        One of the regions listen in 'Region' column of station_df.
    ax : matplotlib.axes.Axes
        The axes, of which the precip axes should be a twin of.
    p_shading : boolean
        Default: True
        If True, times with precip >= 0.1mm/h will be highlighted
        with a blue background color.
    p_intensity : boolean
        Default: True
        Whether the precip-shading should vary with intensity or not.
        If True, the shading will vary as follows:
        lightest_blue >= 0.1mm/h
        light_blue >= 0.5mm/h
        blue >= 4mm/h
        dark_blue >= 10mm/h
    """
    ax2 = ax.twinx()
    # show background of ax2
    ax2.patch.set_visible(True)
    # select current subregion
    data = station_df[station_df.index.get_level_values("Region") == region]
    # drop region level
    data = data.droplevel([0])
    # take only first station of whole region, since precip is the same for all stations within one region
    data = data.loc[[data.index.get_level_values("Messtellenname")[0]]].droplevel(0)
    # calculate 1h pecip sums
    data["N_sum_1hr"] = data["N"].diff()
    data["N_sum_1hr"] = data["N_sum_1hr"].mask(data["N_sum_1hr"] < 0, data["N"])

    # create colors for different precip intensities
    n_colors = 4
    cmap = mpl.colormaps["Blues"]
    # Take colors at regular intervals spanning the colormap.
    colors = cmap(np.linspace(0.15, 0.6, n_colors))

    # if shading is wished
    if p_shading:
        # create dataframe that includes only times where hourly sum is >=0.1mm
        precip_light = data.where(data.N_sum_1hr >= 0.1)  # .dropna()
        precip_light = precip_light[precip_light["N_sum_1hr"].notna()]

        # add background shading depending on precip intensity
        for precip in precip_light.index:
            ax2.axvspan(
                (precip - np.timedelta64(1, "h")),
                precip,
                facecolor=colors[0],
                alpha=0.4,
            )

    # if shading should be dependent on precip intensity
    if p_intensity & p_shading:
        precip_medium = data.where(data.N_sum_1hr >= 0.5)  # .dropna()
        precip_medium = precip_medium[precip_medium["N_sum_1hr"].notna()]

        precip_strong = data.where(data.N_sum_1hr >= 4)  # .dropna()
        precip_strong = precip_strong[precip_strong["N_sum_1hr"].notna()]

        precip_vstrong = data.where(data.N_sum_1hr >= 10)  # .dropna()
        precip_vstrong = precip_vstrong[precip_vstrong["N_sum_1hr"].notna()]

        for precip in precip_medium.index:
            ax2.axvspan(
                (precip - np.timedelta64(1, "h")),
                precip,
                facecolor=colors[1],
                alpha=0.4,
            )
        for precip in precip_strong.index:
            ax2.axvspan(
                (precip - np.timedelta64(1, "h")),
                precip,
                facecolor=colors[2],
                alpha=0.4,
            )
        for precip in precip_vstrong.index:
            ax2.axvspan(
                (precip - np.timedelta64(1, "h")),
                precip,
                facecolor=colors[3],
                alpha=0.4,
            )
    # plot precip timeseries as lineplot
    sns.lineplot(
        data=data.dropna(subset=["N"]),
        x="TIMESTAMP",
        y="N",
        label=data.Messtellenname_N.iloc[0],
        ax=ax2,
        color="black",
        linestyle="--",
    )
    # settings for precip lineplot
    plot_settings(ax2, region, ax_type="precip")
    # configure handles and labels for legend of precip intensities
    handle, label = ax2.get_legend_handles_labels()
    if p_intensity & p_shading:
        # produce handles and labels for 4 different intensities
        handle.extend(
            [
                mpatches.Patch(color=colors[0]),
                mpatches.Patch(color=colors[1]),
                mpatches.Patch(color=colors[2]),
                mpatches.Patch(color=colors[3]),
            ]
        )
        label.extend(
            [
                r"$\mathrm{N} \geq \mathrm{0.1 mm/h}$",
                r"$\mathrm{N} \geq \mathrm{0.5 mm/h}$",
                r"$\mathrm{N} \geq \mathrm{4 mm/h}$",
                r"$\mathrm{N} \geq \mathrm{10 mm/h}$",
            ]
        )
    elif p_intensity == False & p_shading:
        # produce handles and labels only for >=0.1mm intensity
        handle.append(mpatches.Patch(color=colors[0]))
        label.append(r"$\mathrm{N} \geq \mathrm{0.1 mm/h}$")
    return handle, label


def plot(outputpath, station_df, obs_df, p_intensity=True, p_shading=True):
    """
    Takes a pandas-dataframe created by calculate.allstations_DSL() as an input,
    plots the DSLs and precip (line) for each region and saves it to
    [outputpath]/Plots/[timeperiod]/[date]

    Parameters
    ----------
    outputpath : str
        Path where to store all output -
        Plots will be stored in [outputpath]/Plots/[timeperiod]/[date]
    station_df : pandas dataframe
        created by function 'allstations_DSL';
        'Region', 'Messtellenname', 'TIMESTAMP' as indices;
        DSL, N, Messstellennummer,Laenge,Breite,Höhe,Messtellenname_N,
        Höhe_N, Messstellennummer_N as columns.
        The caluclated DSLs.
    obs_df : pandas DataFrame
        created by read_data.read_observerdata(type = 'http').
        'TIMESTAMP' as index, 'Region' and 'DSL' as column.
        The observed DLSs.
    p_shading : boolean
        Default: True
        If True, times with precip >= 0.1mm/h will be highlighted
        with a blue background color.
    p_intensity : boolean
        Default: True.
        If True, the precip shading of the plots will vary
        depending on precip intensity.

    Saves
    -------
    pnt-files : pnt
        Plot of the daily/weekly/monthly/manual DSL;
        all files are named according to their respective
        region and saved into [outputpath]/Plots/[timeperiod]/[date]
    """

    # get the length of the given timeperiod
    date = station_df.index.get_level_values("TIMESTAMP")[0]
    date_end = station_df.index.get_level_values("TIMESTAMP")[-1]
    timedelta = date_end - date

    # create different folders for daily, weekly and monthly plots
    if timedelta.days == 0:
        zeitspanne = "daily"
    elif timedelta.days == 6:
        zeitspanne = "weekly"
    elif (
        timedelta.days == 30
        or timedelta.days == 29
        or timedelta.days == 28
        or timedelta.days == 27
    ):
        zeitspanne = "monthly"
    else:
        zeitspanne = "manual"
    # date as string
    date_strg = date.strftime("%Y-%m-%d")
    date_end_strg = date_end.strftime("%Y-%m-%d")

    # create folders
    results_dir = outputpath + "/Plots/" + zeitspanne + "/" + date_end_strg + "/"
    if zeitspanne == "manual":
        results_dir = (
            outputpath
            + "/Plots/"
            + zeitspanne
            + "/"
            + date_strg
            + "-"
            + date_end_strg
            + "/"
        )
    if not os.path.isdir(results_dir):
        os.makedirs(results_dir)

    # plot for each region
    for region in station_df.index.unique("Region"):
        # plot size according to timeperiod of the data
        if zeitspanne == "monthly" or zeitspanne == "manual" or zeitspanne == "weekly":
            fig, ax = plt.subplots(figsize=(15, 5))
        else:
            fig, ax = plt.subplots(figsize=(9, 5))
        # select data for specified region
        data = station_df[station_df.index.get_level_values("Region") == region]

        # plot DSL of all stations within the region
        sns.lineplot(
            data=data.dropna(),
            x="TIMESTAMP",
            y="DSL",
            hue="Messtellenname",
            ax=ax,
            lw=1.2,
        )
        # plot observerdata
        # if not obsdata.empty:
        if not obs_df.empty:
            region_number = region.split()[0]
            obsdata = obs_df[obs_df.Region == region_number]
            # split obsdata in two: point in time/whole precip event
            if not obsdata.empty:
                obsdata_pointintime = obsdata[
                    obsdata.elevationPeriod == "observationPeriod"
                ]
                obsdata_wholeprecipevent = obsdata[
                    obsdata.elevationPeriod == "duringPrecipitationEvent"
                ]
                # Fehlerbalken auf der y-Achse hinzufügen
                plt.errorbar(
                    obsdata_pointintime.index,
                    obsdata_pointintime.DSL,
                    yerr=obsdata_pointintime.elevationTolerance,
                    color="black",
                    zorder=20,
                    fmt="o",
                    capsize=4,
                    markersize=4,
                    label="zum Beobachtungszeitpunkt",
                )

                # Fehlerbalken auf der y-Achse hinzufügen
                plt.errorbar(
                    obsdata_wholeprecipevent.index,
                    obsdata_wholeprecipevent.DSL,
                    yerr=obsdata_wholeprecipevent.elevationTolerance,
                    color="red",
                    zorder=20,
                    fmt="o",
                    capsize=4,
                    markersize=4,
                    label="gesamtes Niederschlagsereignis",
                )

        plot_settings(ax, region, zeitspanne, station_df=station_df)

        # configure two legends within the plot: one for the stations DSL lineplot
        # one for the precips lineplot and the different precip shadings
        ax.patch.set_visible(False)
        legend1 = ax.legend(
            title="trockene Schneefallgrenze", loc="upper left", fontsize=8
        )

        handle, label = plot_precip(station_df, region, ax, p_intensity, p_shading)
        ax.legend(handle, label, title="Niederschlag", loc="upper right", fontsize=8)
        ax.add_artist(legend1)
        ax.set_zorder(10)

        # save figure
        plt.savefig(
            results_dir + str(region).split(" ")[0] + ".png",
            bbox_inches="tight",
            dpi=300,
        )
        # clear
        fig.clf()
        plt.close("all")


def plot_linreg(df_dsl, df_obs, outputpath):
    """
    Takes two pandas dataframes: one for the calculated DSLs and
    one for the observed DSLs of the same timeperiod,
    uses only observations, where p_int >=0.1mm at the time of the observation,
    takes the median of the respective calculated DSLs of all stations within the regeion
    and plots a linear regression between calculated median DSL and the observed DSL.

    Parameters
    ----------
    df_dsl : pandas dataframe
        created by function calculate.allstations_DSL;
        'Region', 'Messtellenname', 'TIMESTAMP' as indices;
        DSL, N, Messstellennummer,Laenge,Breite,Höhe,Messtellenname_N,Höhe_N, Messstellennummer_N
        as columns
    df_obs : pandas dataframe
        created by read_data.read_observerdata;
        specified timeperiod and regions need to be the same as for df_dsl
    outputpath : str
        Path where to store all output -
        this plot will be stored in [outputpath]/Plots/ISSW/linreg.png
    Saves
    -------
    pnt-file : pnt
        Linear-regression plot;
    """
    # round observation times to next hour
    df_obs.index = df_obs.index.round("h")
    df_obs = df_obs.set_index(["Region", df_obs.index])

    # Extract the current MultiIndex as a list of tuples
    new_index = df_dsl.index.to_frame()
    # Modify the 'Region' part by applying the split operation
    # (keep only the regionnumber, not the name to make the index match the observerdata)
    new_index["Region"] = new_index["Region"].apply(lambda x: x.split()[0])
    # Set the modified index back to the dataframe
    df_dsl.index = pd.MultiIndex.from_frame(new_index)
    # use only DSL where p_int > 0mm/hr and calculate median DSL per region
    df = (
        df_dsl.loc[df_dsl.N_sum_1hr > 0]
        .groupby(level=["Region", "TIMESTAMP"])["DSL"]
        .median()
        .to_frame()
    )
    # merge oberved and calculated DSL
    merged = pd.merge(df, df_obs, left_index=True, right_index=True)
    merged = merged.rename(columns={"DSL_x": "DSL_calc", "DSL_y": "DSL_obs"})

    # plot scatterplot and linear regression, calculate pearson correlation coefficient
    r, p = sp.stats.pearsonr(merged["DSL_obs"], merged["DSL_calc"])
    fig, ax = plt.subplots(figsize=(5, 5))
    sns.lineplot(
        x=np.arange(3001),
        y=np.arange(3001),
        color=plt.rcParams["grid.color"],
        linestyle="--",
        zorder=0,
    )
    sns.scatterplot(data=merged, x="DSL_obs", y="DSL_calc", color="k")
    sns.regplot(data=merged, x="DSL_obs", y="DSL_calc", ci=None, color="k")
    ax.set_axisbelow(True)
    plt.xlim(0, 3000)
    plt.ylim(0, 3000)
    plt.xlabel("SFL observed [m]")
    plt.ylabel("DSL calculated [m]")
    plt.grid(which="major", zorder=-1.0)
    ax = plt.gca()
    ax.text(
        0.01,
        0.9,
        "R² = {:.2f}".format(r),
        transform=ax.transAxes,
        fontstyle="italic",
        fontsize=9,
    )
    ax.tick_params(direction="in", which="both", top=True, right=True)
    plt.savefig(outputpath + "/Plots/ISSW/linreg.png", bbox_inches="tight", dpi=300)


def plot_verlauf_issw(df_dsl, df_obs1, df_obs2, microregion1, microregion2, outputpath):
    """
    Plots a plot with two subplots, each showing for a specific microregion:
    a timeseries plot with the calculated DSLs and the respective observed DSLs as grey dots.

    Parameters
    ----------
    df_dsl : pandas dataframe
        created by function calculate.allstations_DSL;
        'Region', 'Messtellenname', 'TIMESTAMP' as indices;
        DSL, N, Messstellennummer,Laenge,Breite,Höhe,Messtellenname_N,
        Höhe_N, Messstellennummer_N as columns
    df_obs1/2 : pandas dataframe
        created by read_data.read_observerdata;
        specified timeperiod needs to be the same as for df_dsl
    microregion1/2: str
        microregion1/2 needs to be one specific region,e.g. "AT-07-22 Zentrale Stubaier Alpen"
    outputpath : str
        Path where to store all output - this plot will be stored in
        [outputpath]/Plots/ISSW/linreg.png
    Saves
    -------
    pnt-file : pnt
        Timeseries-subplot with two vertically aligned subplots for the two microregions;
    """
    df_dsl1 = df_dsl[df_dsl.index.get_level_values("Region") == microregion1]
    df_dsl2 = df_dsl[df_dsl.index.get_level_values("Region") == microregion2]
    fig, ax = plt.subplots(2, 1, figsize=(12, 5.5))
    plt.rcParams.update({"font.size": 9})

    # subplot top
    sns.lineplot(
        data=df_dsl1.dropna(),
        x="TIMESTAMP",
        y="DSL",
        hue="Messtellenname",
        ax=ax[0],
        lw=1.2,
    )
    sns.scatterplot(
        data=df_obs1.dropna(),
        x="TIMESTAMP",
        y="DSL",
        zorder=20,
        ax=ax[0],
        c="dimgrey",
        label="Observed",
    )

    ax[0].set_ylabel("Elevation [m]", fontdict={"size": 10})
    ax[0].set_xlabel("")
    ax[0].set_xticklabels([])
    # Text in the x-axis will be displayed in 'dd month' format.
    ax[0].xaxis.set_major_locator(mdates.HourLocator(np.arange(0, 24, 24)))
    ax[0].xaxis.set_major_formatter(mpl.dates.DateFormatter("%d %b"))
    ax[0].minorticks_on()
    ax[0].xaxis.set_minor_locator(mdates.HourLocator(np.arange(0, 24, 6)))
    ax[0].set_yticks([1000, 2000, 3000])

    # Set the range of y-axis
    ax[0].set_ylim(0, 3500)
    # Set the range of x-axis
    ax[0].set_xlim(
        [
            df_dsl1.index.get_level_values("TIMESTAMP")[0],
            df_dsl1.index.get_level_values("TIMESTAMP")[-1],
        ]
    )

    # add legends and plot precip
    ax[0].patch.set_visible(False)
    legend1 = ax[0].legend(title="DSL", loc="upper left", fontsize=7, ncol=2)
    legend1.get_title().set_fontsize("8")
    ax[0].set_zorder(0)
    ax[0].add_artist(legend1)
    ax[0].tick_params(direction="in", which="both", top=True, right=True)
    handle, label = plot_precip(df_dsl1, microregion1, ax=ax[0], p_shading=False)
    legend2 = ax[0].legend(
        handle, label, title="Precipitation", loc="upper right", fontsize=7
    )
    legend2.get_title().set_fontsize("8")

    ax[0].text(
        0.03,
        0.12,
        "(A)",
        transform=ax[0].transAxes,
        fontstyle="italic",
        fontsize=14,
        ha="center",
        va="center",
    )

    # subplot bottom
    sns.lineplot(
        data=df_dsl2.dropna(),
        x="TIMESTAMP",
        y="DSL",
        hue="Messtellenname",
        ax=ax[1],
        lw=1.2,
    )
    sns.scatterplot(
        data=df_obs2.dropna(),
        x="TIMESTAMP",
        y="DSL",
        zorder=20,
        ax=ax[1],
        c="dimgrey",
        label="Observed",
    )

    ax[1].set_xlabel("Winter 2023 - 2024")
    ax[1].set_ylabel("Elevation [m]", fontdict={"size": 10})
    # Text in the x-axis will be displayed in 'dd month' format.
    ax[1].xaxis.set_major_locator(mdates.HourLocator(np.arange(0, 24, 24)))
    ax[1].xaxis.set_major_formatter(mpl.dates.DateFormatter("%d %b"))
    ax[1].minorticks_on()
    ax[1].xaxis.set_minor_locator(mdates.HourLocator(np.arange(0, 24, 6)))
    ax[1].set_yticks([1000, 2000, 3000])
    # Set the range of y-axis
    ax[1].set_ylim(0, 3500)
    # Set the range of x-axis
    ax[1].set_xlim(
        [
            df_dsl1.index.get_level_values("TIMESTAMP")[0],
            df_dsl1.index.get_level_values("TIMESTAMP")[-1],
        ]
    )

    # add legends and plot precip
    ax[1].patch.set_visible(False)
    legend1 = ax[1].legend(title="DSL", loc="upper left", fontsize=7, ncol=3)
    legend1.get_title().set_fontsize("8")
    ax[1].set_zorder(0)
    ax[1].add_artist(legend1)
    ax[1].tick_params(direction="in", which="both", top=True, right=True)
    handle, label = plot_precip(df_dsl2, microregion2, ax=ax[1], p_shading=False)
    legend2 = ax[1].legend(
        handle, label, title="Precipitation", loc="upper right", fontsize=7
    )
    legend2.get_title().set_fontsize("8")

    ax[1].text(
        0.03,
        0.12,
        "(B)",
        transform=ax[1].transAxes,
        fontstyle="italic",
        fontsize=14,
        ha="center",
        va="center",
    )
    plt.savefig(
        outputpath + "/Plots/ISSW/ISSW_Verlauf.png", bbox_inches="tight", dpi=300
    )


if __name__ == "__main__":
    stationdata = pd.read_csv(
        "./dsl_calculated.csv", index_col=[0, 1, 2], parse_dates=[2]
    )

    obsdata = pd.read_csv("./dsl_observed.csv", index_col=[0], parse_dates=True)
    plot("./output", stationdata, obsdata, p_intensity=True)
