import argparse

import calculate
import plot
import read_data
import save

# Instantiate the parser
parser = argparse.ArgumentParser(description="")

# Required positional argument: username API request
parser.add_argument(
    "--username_WISKI", type=str, help="The username for the API request."
)

# Required positional argument: password API request
parser.add_argument(
    "--password_WISKI", type=str, help="The password for the API request."
)

# Required positional argument: username API request
parser.add_argument(
    "--username_GUI", type=str, help="The username for the API request."
)

# Required positional argument: password API request
parser.add_argument(
    "--password_GUI", type=str, help="The password for the API request."
)

# Required positional argument: path to input file
parser.add_argument("--input_path", type=str, help="The path to your input file.")
# Optional positional argument: path to ouput
parser.add_argument(
    "--output_path",
    default="./",
    type=str,
    help="The path where geojsons and plots are saved to. Plots will be stored in [path]/Plots/[timeperiod], geojson-files will be stored in [path]/geojson",
)

# Optional positional argument: timeperiod
parser.add_argument(
    "--timeperiod",
    default="w",
    type=str,
    help="Default: y. DSL is calculated, plotted and stored for 24h timeframe of yesterday. Must be either of the following options:  y (yesterday), t (today), w (last week / 7 days), m (last month / 31 days), date1,date2: a manually given period date1/2 given in the form yyyy-mm-dd",
)

# Optional positional argument: p_intensity
parser.add_argument(
    "--p_intensity",
    default="yes",
    type=str,
    help="Default: yes. Precip shading is plotted if p >= 0.1mm/h. Other options: yes (Precip shading colors depend on precip intensity)",
)

# Optional positional argument: tpsy_threshold
parser.add_argument(
    "--tw_threshold",
    default=0.3,
    type=float,
    help="Default: 0.3°C. The wet-bulb temperature theshold used to seperate rain from snow (i.e. the wet-bulb temperature below which we assume all precip to be snow).",
)

stationdata = None


def main():
    """
    Plots last weeks hourly max DSL (line), precip (line) and observed DSL (dots) per region,
    saves the daily maximum DSL with precip >= 0.1mm of each station as 'yyyy-mm-dd.geojson'.
    """
    args = parser.parse_args()
    # read metadata
    print("Loading metadata ...\n")
    # meta = calculate.read_meta(inputpath)
    meta = read_data.read_meta(args.input_path)

    # Toggle this block if you want to calculate and plot only for specified regions
    # # Define the list of names to filter by
    # regions_to_filter = ["AT-07-14-04 Kalkkögel",
    #     "AT-07-02-02 Östliche Lechtaler Alpen",
    #     "AT-07-07 Westliche Lechtaler Alpen",
    #    "AT-07-08 Zentrale Lechtaler Alpen",
    # ]

    # Use the 'isin()' method to filter the DataFrame
    # meta = meta[meta["Region"].isin(regions_to_filter)]

    # read the observed DSLs of the given timeperiod
    print("Loading observerdata ...\n")
    obsdata = read_data.read_observerdata(
        args.username_GUI, args.password_GUI, period=args.timeperiod, type="http"
    )
    obsdata.to_csv("./dsl_observed.csv")

    # calculate yesterdays hourly max DSL for all stations, calculate 6hr precip sums for all stations
    print("Calculating DSLs ...\n")
    stationdata = calculate.allstations_DSL(
        args.username_WISKI,
        args.password_WISKI,
        meta,
        period=str(args.timeperiod),
        tpsy_threshold=args.tw_threshold,
    )
    stationdata.to_csv("./dsl_calculated.csv")

    # save yesterdays daily max DSLs to geojson called 'yyyy-mm-dd.geojson'
    print("Saving geojson ...\n")
    save.save_geojson(stationdata, meta, args.output_path)

    # plot yesterdays DSL and 6hr precip sums per region
    print("Plotting ...\n")
    if args.p_intensity == "yes":
        plot.plot(
            args.output_path, stationdata, obsdata, p_intensity=True, p_shading=True
        )
    if args.p_intensity == "no":
        plot.plot(
            args.output_path, stationdata, obsdata, p_intensity=False, p_shading=True
        )


if __name__ == "__main__":
    main()
