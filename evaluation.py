import argparse

import calculate
import plot
import read_data

# Instantiate the parser
parser = argparse.ArgumentParser(description="")

# Required positional argument: username API request
parser.add_argument("--username", type=str, help="The username for the API request.")

# Required positional argument: password API request
parser.add_argument("--password", type=str, help="The password for the API request.")

# Required positional argument: path to input file
parser.add_argument("--input_path", type=str, help="The path to your input file.")
# Optional positional argument: path to ouput
parser.add_argument(
    "--output_path",
    default="./",
    type=str,
    help="The path where geojsons and plots are saved to. Plots will be stored in [path]/Plots/[timeperiod], geojson-files will be stored in [path]/geojson",
)

# Optional positional argument: timeperiod
parser.add_argument(
    "--timeperiod",
    default="2023-11-01,2024-05-01",
    type=str,
    help="Default: y. DSL is calculated, plotted and stored for 24h timeframe of yesterday. Must be either of the following options:  y (yesterday), t (today), w (last week / 7 days), m (last month / 31 days), date1,date2: a manually given period date1/2 given in the form yyyy-mm-dd",
)

# Optional positional argument: tpsy_threshold
parser.add_argument(
    "--tw_threshold",
    default=0.3,
    type=float,
    help="Default: 0.3°C. The wet-bulb temperature threshold used to seperate rain from snow (i.e. the wet-bulb temperature below which we assume all precip to be snow).",
)

parser.add_argument(
    "--inputpath_obs",
    default="./input/Beobachterdaten_Regengrenzen_Tirol_23-24.xlsx",
    type=str,
    help="Default: ./input/Beobachterdaten_Regengrenzen_Tirol_23-24.xlsx. The path to the observerdata-file. Observerdata must be an excel file.",
)

parser.add_argument(
    "--eval_plottype",
    default="linreg",
    type=str,
    help="Default: linreg. Plots a linear regression between calculated DSL and observed snowfall limits. Other options: progression. Plots a progression-plot of two selected microregions and a selected timeperiod.",
)

parser.add_argument(
    "--microregion1",
    default="AT-07-15 Westliche Tuxer Alpen",
    type=str,
    help="Default: AT-07-15 Westliche Tuxer Alpen. Only needed for progression plot. If --eval_plottype == progression. The first microregion the progression is plotted for.",
)

parser.add_argument(
    "--microregion2",
    default="AT-07-22 Zentrale Stubaier Alpen",
    type=str,
    help="Default: AT-07-22 Zentrale Stubaier Alpen. Only needed for progression plot. If --eval_plottype == progression. The second microregion the progression is plotted for.",
)

stationdata = None


def main():
    """
    Plots a linear regression between observed snowfall limits and calculated DSLs /
    a progression plot for two specific microregions, showing calculated DSLs for
    all stations within the region, precipiation and observed snowfall limits as grey dots.
    """
    args = parser.parse_args()
    # read metadata
    print("Loading metadata ...\n")
    # meta = calculate.read_meta(inputpath)
    meta = read_data.read_meta(args.input_path)

    # calculate yesterdays hourly max DSL for all stations, calculate 6hr precip sums for all stations
    print("Calculating DSLs ...\n")
    stationdata = calculate.allstations_DSL(
        args.username,
        args.password,
        meta,
        period=str(args.timeperiod),
        tpsy_threshold=args.tw_threshold,
    )

    # plot yesterdays DSL and 6hr precip sums per region
    print("Plotting ...\n")
    if args.eval_plottype == "linreg":
        observerdata = read_data.read_observerdata(args.inputpath_obs)
        # plot the linear regression
        plot.plot_linreg(stationdata, observerdata, args.output_path)
    if args.eval_plottype == "progression":
        df_obs1 = read_data.read_observerdata(
            args.inputpath_obs, microregion=args.microregion1, period=args.timeperiod
        )
        df_obs2 = read_data.read_observerdata(
            args.inputpath_obs, microregion=args.microregion2, period=args.timeperiod
        )
        plot.plot_verlauf_issw(
            stationdata,
            df_obs1,
            df_obs2,
            args.microregion1,
            args.microregion2,
            args.output_path,
        )


if __name__ == "__main__":
    main()
