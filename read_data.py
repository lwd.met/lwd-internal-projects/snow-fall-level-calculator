# import libaries
import datetime
import io
from datetime import timedelta

import numpy as np
import pandas as pd

# for http request
import requests

import calculate


def read_meta(metadatapath):
    """
    Reads the stations metadata, that needs to be an excel-file
    consisting of the following parameters:
    Messtellennummer, Messtellenname,Laenge, Breite, Höhe, Region,
    Messtellenname_N (zugehöriger Niederschlagstop), Höhe_N, Messstellennummer_N

    Parameters
    ----------
    metadatapath : str
        The path to the excel file.

    Returns
    -------
    meta : pandas dataframe
        dataframe of all stations that are to be used for the DSL calculation
        with Messstellennummer as index, Messtellenname, Laenge, Breite, Höhe,
        "Messtellenname_N", "Höhe_N" and "Messtellennummer_N"
        of the respective closest precip station for each LWD station as columns
    """
    meta = pd.read_excel(metadatapath, index_col=0, engine="openpyxl")
    meta.index = meta.index.astype(str)
    return meta


def http_to_pandas(user, password, pgnr, parameter, start, ende):
    """
    Executes an url request of the WISKI-API interface and
    returns the requested data as a pandas-dataframe.

    Parameters
    ----------
    user : str
        username for url-request of WISKI-API interface
    password : str
        password for url-request of WISKI-API interface
    pgnr : int
        stationnumber
    parameter : str
        one of the following:
        N (precipitation), LT (air-temperature), LF (relative humidity)
    start : datetime
        Starting date as datetime object
    ende : datetime
        End date as datetime object

    Returns
    -------
    df : pandas dataframe
        10-min dataframe with datetime as index, LT and LF as column
        if precip == True:
        1-hour dataframe with datetime as index,
        N as column (aggregated psum, reset daily 7:00am)
    """
    url = (
        "https://wiski-daten.tirol.gv.at/timeseries?modus=CSV&pgnr="
        + str(pgnr)
        + "&parameter="
        + str(parameter)
        + "&beginn="
        + str(start)
        + "&ende="
        + str(ende)
        + "&user="
        + str(user)
        + "&pass="
        + str(password)
    )
    response = requests.get(url)
    urlData = requests.get(url).content
    colnames = ["TIMESTAMP", parameter]

    if response.status_code == 200:
        df = pd.read_csv(
            io.StringIO(urlData.decode("utf-8")),
            index_col=0,
            header=None,
            sep=";",
            names=colnames,
        )
        df.index = pd.to_datetime(df.index)
    else:
        # print(
        #    f"Wetterstation: {str(pgnr)}, Fehler: {response.status_code} - {response.text}"
        # )
        df = pd.DataFrame(
            {
                "TIMESTAMP": pd.date_range(
                    start, ende + timedelta(days=1), freq="1h", inclusive="left"
                ),
                parameter: np.nan,
            }
        )
        df = df.set_index("TIMESTAMP")
    return df


def get_data(user, password, pgnr, start, ende, precip=False):
    """
    Uses the function http_to_pandas to read LT(temperature) and
    LF (relative humidity) datasets (precip N if precip is set to True)
    for the given station, merges the two datasets
    into one pandas dataframe which is returned

    Parameters
    ----------
    user : str
        username for url-request of WISKI-API interface
    password : str
        password for url-request of WISKI-API interface
    pgnr : int
        stationnumber
    start : datetime
        Starting date as datetime object
    ende : datetime
        End date as datetime object
    precip : boolean
        False by default (if you want to get LT and LF)
        True: if you want to get N data

    Returns
    -------
    df : pandas dataframe
        10-min dataframe with datetime as index, LT and LF as variables
        if precip == True:
        1-hour dataframe with datetime as index,
        N as variable (aggregated psum, reset daily 7:00am)
    """
    if precip:
        df = http_to_pandas(user, password, pgnr, "N", start, ende)
    else:
        # get Temperatur, Luftfeuchtigkeit
        df_LT = http_to_pandas(user, password, pgnr, "LT", start, ende)
        df_LF = http_to_pandas(user, password, pgnr, "LF", start, ende)
        # merge Temperatur und Feuchte Dataframes
        df = pd.merge(df_LT, df_LF, left_index=True, right_index=True)

    return df


def last_sunday_of_month(year, month):
    """
    Returns the last sunday of a specific month in a specific year.

    Parameters
    ----------
    year : int
        The year for which the last sunday should be returned,
        e.g. 2024.
    month : int
        The month for which the last sunday should be returned,
        e.g. 3 for March.

    Returns
    -------
    last_sunday : datetime.date
        The date of the last sunday of the specified
        year and month as a datetime.date object.
    """
    # The last day of March
    last_day_of_march = datetime.date(year, month, 31)
    last_day_of_march_weekday = last_day_of_march.weekday()
    # Calculate the last Sunday
    # if the last day is as sunday
    if last_day_of_march_weekday == 6:
        last_sunday = last_day_of_march
    # if the last day is between monday (last_day_of_march_weekday == 0) and saturday (last_day_of_march_weekday == 5)
    else:
        last_sunday = last_day_of_march - datetime.timedelta(
            days=last_day_of_march.weekday() + 1
        )
    return last_sunday


def change_summer_to_wintertime(df):
    # get the different years included in df
    df_years = df.index.year.unique().tolist()
    # for each year in df
    for year in df_years:
        # get the date of summer- and wintertime start
        start_summertime = last_sunday_of_month(year, 3)
        start_wintertime = last_sunday_of_month(year, 10)
        # create mask for all data that is in summertime (>=start_summertime and <= start_wintertime)
        mask = (df.index >= start_summertime.strftime("%Y-%m-%d")) & (
            df.index < start_wintertime.strftime("%Y-%m-%d")
        )
        # subtract one hour from the masked data
        df.index = (*df[~mask].index, *(df[mask].index + timedelta(hours=-1)))
    df.index.name = "TIMESTAMP"
    return df


def read_observerdata(
    username, password, inputpath=None, microregion=None, period=None, type="excel"
):
    """
    By default, reads an excel file that consists of observed snowfall limits
    and the columns "Region", "Timestamp", "DSL".
    If type = 'http': loads the observed data within
    the specified timeperiod with an http-request.
    Changes the date of all observations to wintertime to make the data comparable
    to AWS data (which is given in wintertime all year round).
    If specified, the function selects the specified region and timeperiod.
    Returns the observed data as a pandas dataframe.

    Parameters
    ----------
    username : str
        username for url-request of admin.gui interface
    password : str
        password for url-request of admin.gui interface
    inputpath : str
        Default: None
        The path to the observerdata-file of the season,
        e.g. './input/Beobachterdaten_Regengrenzen_Tirol_23-24.xlsx'.
        Not specified for type = 'http'.
    microregion : str
        Default: None
        the microregion that will be selected,
        e.g. AT-07-01 for Allgäuer Alps.
    period : str
        Default: None
        The timeperiod that will be selected in the format
        'YYYY-MM-DD,YYYY-MM-DD' ('startdate,enddate')
        Needs to be specified for type = 'http'.
    type : str
        Default: 'excel'. Will read the observerdata from an excel-file.
        Other options: 'http'. Will load the observerdata with an http-request.

    Returns
    -------
    obs : pandas dataframe
        the (selected) observed snowfall limits
    """
    if type == "excel":
        obs = pd.read_excel(inputpath)
        obs.TIMESTAMP = pd.to_datetime(obs.TIMESTAMP)
        obs = obs.set_index("TIMESTAMP")
        if microregion != None:  # noqa: E711
            obs = obs[obs["Region"] == microregion.split()[0]]
        if period != None:  # noqa: E711
            start = pd.to_datetime(period.split(",")[0])
            end = pd.to_datetime(period.split(",")[1])
            obs = obs[start:end]
        obs = obs[["Region", "DSL"]]
        obs = change_summer_to_wintertime(obs)
    elif type == "http":
        mytoken = get_token(username, password)
        data = http_get_observations(mytoken, period)
        obs = filter_for_snowline(data)
    return obs


def get_token(username, password):
    """
    Returns the authorization token for the admin.avalanche.report API.

    Parameters
    ----------
    username : str
        username for url-request of admin.gui interface
    password : str
        password for url-request of admin.gui interface
    Returns
    -------
    auth_token : str
        The authorization token.
    """
    # URL der API
    url = "https://admin.avalanche.report/albina_dev/api/authentication"

    # Nutzlast (Payload) für den POST-Request
    payload = {"username": username, "password": password}

    # Sende den POST-Request
    response = requests.post(url, json=payload)

    # Überprüfe den Statuscode der Antwort
    if response.status_code == 200:
        # Extrahiere den Authentifizierungstoken
        auth_token = response.json().get("access_token")
    else:
        print(f"Fehler: {response.status_code} - {response.text}")
    return auth_token


def http_get_observations(token, period):
    """
    Returns all observerdata for the specified period from an http-request.

    Parameters
    ----------
    token : str
        Output of get_token(). The authorization token
        for the admin.avalanche.report API.
    period : str
        must be either of the following options:
        't' (today), 'w' (last week / 7 days),
        'm' (last month / 31 days)
        'date1,date2': a manually given period date1/2
        given in the form yyyy-mm-dd

    Returns
    -------
    data : list
        List with each list-element being a single observation.
    """
    startdate, enddate = calculate.period_to_startend(period)
    # URL der API
    url = (
        "https://admin.avalanche.report/albina_dev/api_ext/observations?startDate="
        + startdate.strftime("%Y-%m-%d")
        + "T00:00:00.000Z&endDate="
        + enddate.strftime("%Y-%m-%d")
        + "T23:59:59.999Z"
    )
    headers = {"Authorization": f"Bearer {token}"}
    # Sende GET-Request
    response = requests.get(url, headers=headers)
    # Überprüfe den Statuscode der Antwort
    if response.status_code == 200:
        # Observations stored in data as a list (each observation)
        # of dictionaries (data of one observation)
        data = response.json()
    else:
        print(f"Fehler: {response.status_code} - {response.text}")
    return data


def filter_for_snowline(obsdata):
    """
    Returns the observed data filtered for 'SnowLine' observations,
    in wintertime, as a pd.DataFrame with 'TIMESTAMP' as index,
    'Region' and 'DSL' as columns.

    Parameters
    ----------
    obsdata : list
        Output of http_get_observations().
        The observed data for the given timeperiod as a list.

    Returns
    -------
    df_filtered : pandas DataFrame
        The observed 'SnowLine' data for the given timeperiod,
        as a pandas DataFrame and in wintertime.
    """
    # filter for data with SnowLine as entry of importantObservations
    filtered_data = [
        entry for entry in obsdata if entry.get("importantObservations") == ["SnowLine"]
    ]
    # filter for data with SimpleObservation as entry of $type
    filtered_data = [
        entry for entry in filtered_data if entry.get("$type") == "SimpleObservation"
    ]
    # delete entrys of calculated DSLs
    filtered_data = [entry for entry in filtered_data if entry["$source"] != "SnowLine"]
    # Überprüfen, ob Einträge vorhanden sind
    if len(filtered_data) > 0:
        # Liste für die aufbereiteten Daten
        flattened_data = []
        # Daten flach machen
        for observation in filtered_data:
            flat_observation = observation.copy()  # Originaldaten kopieren
            if "$data" in flat_observation:
                # '$data' mit den anderen Werten zusammenführen
                flat_observation.update(flat_observation.pop("$data", {}))
            if "position" in flat_observation:
                flat_observation.update(flat_observation.pop("position", {}))
            if "adsRegion" in flat_observation:
                if flat_observation["adsRegion"] == None:  # noqa: E711
                    pass
                else:
                    flat_observation.update(flat_observation.pop("adsRegion", {}))
            flattened_data.append(flat_observation)
            # In ein DataFrame umwandeln
        df_filtered = pd.DataFrame(flattened_data)
        # filter for relevant columns
        df_filtered = df_filtered[
            [
                "eventDate",
                "loc_ref",
                "elevation",
                "elevationPeriod",
                "elevationTolerance",
                "authorName",
            ]
        ]
        df_filtered.rename(
            columns={"eventDate": "TIMESTAMP", "loc_ref": "Region", "elevation": "DSL"},
            inplace=True,
        )
        # make evenDate a datetime object
        df_filtered.TIMESTAMP = pd.to_datetime(df_filtered.TIMESTAMP)
        # set eventDate as index and rename to 'TIMESTAMP'
        df_filtered.set_index("TIMESTAMP", inplace=True)
        # Umwandeln in MEZ (Winterzeit)
        df_filtered.index = df_filtered.index.tz_convert("Europe/Berlin")
        df_filtered.index = df_filtered.index.tz_localize(None)
        # sort by time
        df_filtered = df_filtered.sort_index()
        # change all data to wintertime
        df_filtered = change_summer_to_wintertime(df_filtered)
        # drop rows that include NaN values in any of the columns
        df_filtered = df_filtered.dropna()
        if(not df_filtered.empty):
            # For the ElevationTolerance:
            # Remove non-numeric characters and convert to integer (e.g. 50m to 50)
            df_filtered["elevationTolerance"] = df_filtered[
                "elevationTolerance"
            ].str.replace(r"\D", "", regex=True)
            # replace 'exact' with 0
            df_filtered["elevationTolerance"] = df_filtered["elevationTolerance"].replace(
                "", 0
            )
            df_filtered["elevationTolerance"] = df_filtered["elevationTolerance"].astype(
                int
            )
    else:
        print("Keine Einträge mit 'importantObservations' = ['SnowLine'] gefunden.")
        # create empty dataframe
        df_filtered = pd.DataFrame(columns=["Region", "DSL"])
        df_filtered.index.name = "TIMESTAMP"
    return df_filtered


# if __name__ == "__main__":
# obs_data = read_observerdata('./input/Beobachterdaten_Regengrenzen_Tirol_23-24.xlsx')
# mytoken = get_token()
# obs = http_get_observations(mytoken, '2024-03-20,2024-04-20')
# df_obs = filter_for_snowline(obs)
# df_obs = read_observerdata(period="2024-03-22,2024-04-02", type="http")
